#!/bin/bash

echo Creating directory \'urdf\'
mkdir -p dreamer_no_left_arm/urdf

echo Generating dreamer_no_left_arm/urdf/dreamer_no_left_arm.urdf
rosrun xacro xacro.py dreamer_no_left_arm/xacro/dreamer_no_left_arm.xacro -o dreamer_no_left_arm/urdf/dreamer_no_left_arm.urdf

echo Done!
