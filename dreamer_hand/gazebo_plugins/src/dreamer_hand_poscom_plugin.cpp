#include <boost/bind.hpp>
#include <stdio.h>
#include <math.h>
#include <vector>
#include <map>
#include <stdexcept>

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <gazebo/math/Angle.hh>

#include "ros/ros.h"
#include "ros/time.h"
#include "std_msgs/Header.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Float64MultiArray.h"
#include "sensor_msgs/JointState.h"

#include <XmlRpcValue.h>
#include <XmlRpcException.h>

#define JOINT_STATE_PUBLISH_FREQ_HZ 400
#define TOLERANCE 1e-6

// namespace gazebo {
//   namespace common {
//     #define gzout (gazebo::common::Console::Instance()->ColorErr("Dbg",__FILE__, __LINE__,36))
//     #define gzprint (gazebo::common::Console::Instance()->ColorErr("FYI:",__FILE__, __LINE__,25))
//   }
// }

namespace gazebo {
  class DreamerHandPosComPlugin : public ModelPlugin
  {

    public: DreamerHandPosComPlugin()
    {
      // Start up ROS
      std::string name = "dreamer_hand_poscom_plugin";
      int argc = 0;
      ros::init(argc, NULL, name);

      lastPubTime = ros::Time::now();

      BASE_JOINT_NAMES = new std::string[5] {
        "m3joint_ua_mh8_j0",
        "m3joint_ua_mh8_j1",
        "m3joint_ua_mh8_j3",
        "m3joint_ua_mh8_j6",
        "m3joint_ua_mh8_j9"
      };
    }

    public: ~DreamerHandPosComPlugin()
    {
      delete this->node;
      // delete[] this->JOINT_NAMES;
    }

    public: void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
    {
      // Store the pointer to the model
      this->model = _parent;
      physics::Joint_V allJoints = this->model->GetJoints();
      std::vector<std::string> joint_names;

      for (size_t ii = 0; ii < allJoints.size(); ii++)
      {
        // Pick out non-fixed joints
        if(allJoints[ii]->GetLowerLimit(0).Radian() < -TOLERANCE && allJoints[ii]->GetUpperLimit(0).Radian() > TOLERANCE)
        {
          joints.insert(make_pair(allJoints[ii]->GetName(), allJoints[ii]));
          joint_names.push_back(allJoints[ii]->GetName());
        }

      }

      // TODO : CHECK base joints are in all joints!!!
      // TODO : Get KP/KD from parameters at startup!!!

      // Set up parameters for this plugin
      for (size_t ii = 0; ii < 5; ii++)
      {
          physics::JointPtr _jptr = this->model->GetJoint(BASE_JOINT_NAMES[ii]);
          base_joints.insert(std::make_pair(BASE_JOINT_NAMES[ii], _jptr));
          kp.insert(std::make_pair(BASE_JOINT_NAMES[ii], 1.0));
          kd.insert(std::make_pair(BASE_JOINT_NAMES[ii], 0.01));
          goalPosition.insert(std::make_pair(BASE_JOINT_NAMES[ii],0.0));
          // map to be copied into transmisions, all 1 for now...possibly parse this from urdf?
          std::map<std::string, double> tmap;
          if(ii>0)
          {
            auto jit = std::find(joint_names.begin(), joint_names.end(), BASE_JOINT_NAMES[ii]);
            jit++;
            if(ii==1)
              tmap.insert(make_pair(*jit, 0.185));
            else
              tmap.insert(make_pair(*jit, 0.945));
            if(ii > 1)
            {
              jit++;
              tmap.insert(make_pair(*jit, 0.971));
            }
          }
          transmission.insert(make_pair(BASE_JOINT_NAMES[ii], tmap));
      }

      // print transmission map
      for(auto & base_jnt : base_joints)
      {
        gzdbg << "Joint : " << base_jnt.first << " has " << transmission[base_jnt.first].size() << " slave joints \n";
        for( auto & slave_jnt : transmission[base_jnt.first])
          gzdbg << "\t " << slave_jnt.first << " factor : " << slave_jnt.second << "\n";
      }



      // ROS Nodehandle
      this->node = new ros::NodeHandle;

      // Listeners for published updates to embedded controller
      this->jointPositionGoalSub = this->node->subscribe("right_hand_goal_position", 1000, &DreamerHandPosComPlugin::GoalJointPositionCallback, this);
      this->kpSub = this->node->subscribe("right_hand_kp", 1000, &DreamerHandPosComPlugin::KpCallback, this);
      this->kdSub = this->node->subscribe("right_hand_kd", 1000, &DreamerHandPosComPlugin::KdCallback, this);

      // For publishing the robot's current state
      // this->pub = this->node->advertise<sensor_msgs::JointState>("robot_state", 1000);

      // For subscribing to the command issued by the controller
      // this->subJointState = this->node->subscribe<sensor_msgs::JointState>("command", 1000,
      //   &DreamerHandPosComPlugin::commandCallbackJointState, this);

      // this->subMultiArray = this->node->subscribe<std_msgs::Float64MultiArray>("command_array", 1000,
      //   &DreamerHandPosComPlugin::commandCallbackMultiArray, this);

      // Listen to the update event. This event is broadcast every
      // simulation iteration.
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(
          boost::bind(&DreamerHandPosComPlugin::onUpdate, this));
    }

    public: void GoalJointPositionCallback(const std_msgs::Float64MultiArray goal_msg)
    {
        int ii = 0;
        for (auto & jnt : base_joints)
            goalPosition[jnt.first] = goal_msg.data[ii++];

        gzdbg << "Right Hand Goal Position callback\n";
    }

    public: void KpCallback(const std_msgs::Float64MultiArray gain_msg)
    {
        int ii = 0;
        for (auto & jnt : base_joints)
            kp[jnt.first] = gain_msg.data[ii++];

        gzdbg << "Right Hand Kp callback\n";
    }

    public: void KdCallback(const std_msgs::Float64MultiArray gain_msg)
    {
        int ii = 0;
        for (auto & jnt : base_joints)
            kd[jnt.first] = gain_msg.data[ii++];

        gzdbg << "Right Kd Position callback\n";
    }

    /*!
     * Periodically called by Gazebo during each cycle of the simulation.
     */
    public: void onUpdate()
    {
      // Process pending ROS messages, etc.
      ros::spinOnce();

      // // A message for holding the robot's current state
      // sensor_msgs::JointState currRobotState;
      // currRobotState.header.stamp = ros::Time::now();

      // Update the torque commands and fill in the joint state message
      for(auto & master_jnt : transmission)
      {
        physics::JointPtr master = base_joints[master_jnt.first];
        double masterAngle = master->GetAngle(0).Radian();
        double masterVelocity = master->GetVelocity(0);
        double masterTorque = kp[master_jnt.first] * (goalPosition[master_jnt.first] - masterAngle)
                              - kd[master_jnt.first] * masterVelocity;
        master->SetForce(0,masterTorque);

        int scale = 1;
        for( auto & slave_jnt : master_jnt.second)
        {
          physics::JointPtr slave = joints[slave_jnt.first];
          double slaveAngle = slave->GetAngle(0).Radian();
          double slaveVelocity = slave->GetVelocity(0);
          double slaveTorque = kp[master_jnt.first] * (slave_jnt.second * goalPosition[master_jnt.first] - slaveAngle)
                               + kd[master_jnt.first] * (0 * slave_jnt.second * masterVelocity - slaveVelocity);
          slave->SetForce(0, slaveTorque * pow(0.1,scale++));
        }
      }

      // std::map<std::string, physics::JointPtr>::iterator name_it;
      // for(name_it = joints.begin(); name_it != joints.end(); name_it++)
      // {
      //   std::string jointName = name_it->first;
      //   double effort = torques[jointName];

      //   // Update the joint torque command
      //   name_it->second->SetForce(0, effort);

      //   // Save the joint state
      //   currRobotState.name.push_back(jointName);
      //   currRobotState.position.push_back(name_it->second->GetAngle(0).Radian());
      //   currRobotState.velocity.push_back(name_it->second->GetVelocity(0));
      //   currRobotState.effort.push_back(effort);
      // }

      // // Publish the robot's current state if the appropriate
      // // amount of time has passed

      // if ((currRobotState.header.stamp - lastPubTime).toSec() > 1.0 / 400.0)
      // {
      //   this->pub.publish(currRobotState);
      //   lastPubTime = currRobotState.header.stamp;
      // }
    }

    // Pointer to the model
    private: physics::ModelPtr model;

    // Pointer to the update event connection
    private: event::ConnectionPtr updateConnection;

    // ROS Nodehandle
    private: ros::NodeHandle * node;

    // ROS Subscriber for JointState messages
    ros::Subscriber subJointState;

    // ROS subscriber for Float64MultiArray messages
    // ros::Subscriber subMultiArray;

    // ROS Publisher
    ros::Publisher pub;

    // Store joint pointers in a map indexed by the joint name interpretted from the URDF file

  private:
    // Stores a pointer to each joint
    std::map<std::string, physics::JointPtr> joints;
    std::map<std::string, physics::JointPtr> base_joints;
    std::map<std::string, std::map<std::string, double>> transmission;

    // Embedded control parameters (for base joints)
    std::map<std::string, double> kp;
    std::map<std::string, double> kd;
    std::map<std::string, double> goalPosition;

    // Subscribers to ros topics for hand controls
    ros::Subscriber kpSub;
    ros::Subscriber kdSub;
    ros::Subscriber jointPositionGoalSub;

    // The time when the robot's state was last published
    ros::Time lastPubTime;

    // Hard code the joint names.  TODO: Obtain this via a ROS service call to the controller.
    std::string * BASE_JOINT_NAMES;
  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(DreamerHandPosComPlugin)
}
