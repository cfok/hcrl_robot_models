#!/bin/bash

echo Creating directory \'urdf\'
mkdir -p dreamer/urdf

echo Generating dreamer/urdf/dreamer.urdf
rosrun xacro xacro.py dreamer/xacro/dreamer.xacro -o dreamer/urdf/dreamer.urdf

echo Done!